# Breaking pandas

Material of the [PyLondinium](https://pylondinium.org) talk [Breaking pandas](https://pylondinium.org/talks/talk-24.html).

## Abstract

pandas is more than 10 years old now. In this time, it became almost a standard for building data
pipelines and perform data analysis in Python. As the popularity of the project grows, it also
grows the number of projects that depend or interact with pandas.

This talk will cover pandas architecture, API and how pandas is changing over time. Also the ecosystem
of projects around pandas, mainly in the prespective of scalability and performance. Discussing for
example how projects like Arrow are key for the future of pandas, or how Dask is overcoming pandas limitations.

In a first part, the talk will focus on pandas itself, its components, and its architecture. Explaining how
pandas is broken, and how we can break pandas even more to make it better. This will give the required context
for a second part, that will explain related projects, how they interact with pandas, and what the whole
ecosystem can offer to users.

## Speaker

Marc Garcia is a pandas core developer and Python fellow.

He has been working in Python for more than 12 years, and worked as data scientist and data engineer
for different companies such as Bank of America, Tesco and Badoo.

He is a regular speaker at PyData and PyCon conferences, and a regular organizer of sprints.

[Website](https://datapythonista.github.io) |
[Twitter](https://twitter.com/datapythonista) |
[LinkedIn](https://www.linkedin.com/in/datapythonista/)

## Set up

You can run the slides online using Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/datapythonista/breaking-pandas.git/master)

Or you can install it locally:

- Install [Miniconda 3.7](https://docs.conda.io/en/latest/miniconda.html)
- Open an Anaconda/UNIX terminal
- `git clone https://github.com/datapythonista/breaking-pandas.git`
- `cd breaking-pandas`
- `conda env create`
- `source activate breaking-pandas` (in Windows: `conda activate breaking-pandas`)
- `jupyter notebook`
- Open `breaking_pandas.ipynb` notebook
- Click the icon with the bar plot to show as slides with [RISE](https://damianavila.github.io/RISE/)
